﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juntadas.Model
{
    public class JuntadaAction
    {
        public Juntada Juntada
        {
            get;
            set;
        }
        public Member ActiveMember
        {
            get;
            set;
        }
        public UserStageChoices Choices
        {
            get;
            set;
        }

    }
}
