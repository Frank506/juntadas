﻿using System.Collections.Generic;

namespace Juntadas.Model
{
    // the best schedule coordination that can be computed from members choices
    public class StageResult
    {
        public int StageResultId
        {
            get;
            set;
        }
        public Stage Stage
        {
            get;
            set;
        }
        public List<TimePeriod> TimePeriod
        {
            get;
            set;
        }
    }
}