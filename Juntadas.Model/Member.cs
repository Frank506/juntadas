﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juntadas.Model
{
    public class Member
    {
        public int MemberId
        {
            get;
            set;
        }
        public int UserId
        {
            get;
            set;
        }
        public int JuntadaId
        {
            get;
            set;
        }
        public string MemberName
        {
            get;
            set;
        }
        public bool Host
        {
            get;
            set;
        }
        public bool CanBeAbsent
        {
            get;
            set;
        }
        public bool StageCompleted
        {
            get;
            set;
        }

    }
}
