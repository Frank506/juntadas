﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juntadas.Model
{
    public class Juntada
    {
        public int JuntadaId
        {
            get;
            set;
        }
        public Preset Preset
        {
            get;
            set;
        }
        public Stage Stage
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Address
        {
            get;
            set;
        }
        public List<Member> Members
        {
            get;
            set;
        }
        public DateTime DaysRangeFrom
        {
            get;
            set;
        }
        public DateTime DaysRangeTo
        {
            get;
            set;
        }
        public TimeSpan Duration
        {
            get;
            set;
        }
        public bool Timeless
        {
            get;
            set;
        }
        public bool RecievePersonaly
        {
            get;
            set;
        }
        public bool OtherHosts
        {
            get;
            set;
        }
        public bool LateArrivals
        {
            get;
            set;
        }
        public TimeSpan LateArrivalsLateness
        {
            get;
            set;
        }
        public bool Absences
        {
            get;
            set;
        }
        public List<Member> MembersThatCanBeAbsent
        {
            get;
            set;
        }
        public int HowManyAbsences
        {
            get;
            set;
        }
        // If true, the Juntada is considered either succes/fail, can't be modified and it's history doesn't change anymore
        public bool ClosedJuntada
        {
            get;
            set;
        }
    }
}
