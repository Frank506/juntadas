﻿namespace Juntadas.Model
{
    public class TimePeriod
    {
        public int TimePeriodId
        {
            get;
            set;
        }
        public Stage Stage
        {
            get;
            set;
        }
        public Juntada Juntada
        {
            get;
            set;
        }
        public TimePeriodType Type
        {
            get;
            set;
        }
    }
}