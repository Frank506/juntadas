﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juntadas.Model
{
    public class StageCheckResult
    {
        public int ResultId
        {
            get;
            set;
        }
        public Stage CurrentStage
        {
            get;
            set;
        }
    }
}
