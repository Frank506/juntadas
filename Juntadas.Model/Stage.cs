﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juntadas.Model
{
    public class Stage
    {
        public int StageId
        {
            get;
            set;
        }
        public int StageTypeId
        {
            get;
            set;
        }
        public string StageName
        {
            get;
            set;
        }
        public int StageRound
        {
            get;
            set;
        }
        public bool FinalStage
        {
            get;
            set;
        }
        public List<Member> Members
        {
            get;
            set;
        }
        public StageResult StageResult
        {
            get;
            set;
        }
    }
}
