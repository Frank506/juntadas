﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juntadas.Model
{
    public class User
    {
        public int UserId
        {
            get;
            set;
        }
        public int UserRoleId
        {
            get;
            set;
        }
        public int UserProfileId
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }

    }
}
