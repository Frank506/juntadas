﻿using System;

namespace Juntadas.Model
{
    public class TimePeriodType
    {
        public int TimePeriodTypeId
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public DateTime Start
        {
            get;
            set;
        }
        public DateTime End
        {
            get;
            set;
        }
    }
}