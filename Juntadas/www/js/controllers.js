angular.module('app.controllers', [])

    .controller('homeCtrl', ['$scope', '$stateParams', '$state', '$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $state, $http) {

            // checking connection with WebApi succeeds
            $http.get('http://localhost:50026/Api/Juntadas/Check').success(function (data) {
                console.log(data);
            });

        }])

    .controller('addJuntadaCtrl', ['$scope', 'newJuntadaService',
        function ($scope, newJuntadaService) {
            $scope.juntada = newJuntadaService.getCurrentNewJuntada();
            $scope.guests = $scope.juntada.guests;
            
        }])

    .controller('addJuntadaLogicStepCtrl', ['$scope', 'newJuntadaService', '$http',
        function ($scope, newJuntadaService, $http) {

            

            // TODO: decidir si tener toda la data en $scope y luego pasarla a $scope.juntada o si ir ingresando todo a $scope.juntada desde el principio, por ahora se siguen ambos metodos de manera ad hoc
            $scope.juntada = newJuntadaService.getCurrentNewJuntada();

            // updates daysRangeTo in scope in order to show the complete days range in UI. TODO: va en un service?
            $scope.updateDate = function (from) {
                from = moment(from);
                var to = moment(from, "DD-MM-YYYY").add("days", 6).toDate();;
                $scope.daysRangeTo = to;
            }

            $scope.presetSelection = '';

            $scope.daysRangeTo = '';

            $scope.durationHours = 0;
            $scope.durationMinutes = 0;
            var submited = false;
            $scope.submit = function () {
                // Ionic 1 Bug: ng-click runs twice. Woraround: try catch block that catches the error produced from a second run of the function with an empty scope (clered at the end of the first run)
                try {
                    // Prepare data for submitting. TODO: mover esta logica a un service o averiguar cual es la mejor manera de normalizar la data
                    $scope.juntada.duration.timeSpan = moment($scope.juntada.duration.timeSpan).format('hh:mm');

                    $.post('http://localhost:50026/Api/Juntadas/Init/', $scope.juntada, function (success) {
                        console.log('success! ' + JSON.stringify(success));

                    }, 'json');

                    //$.ajax('http://localhost:50026/Api/Juntadas/Init/', { method: 'POST', data: $scope.juntada, success: function (s) { console.log(s) } });

                    // delete singleton, delete data stored in $scope and clear all form inputs after submiting it's data
                    //newJuntadaService.deleteCurrentNewJuntada();
                    $scope.juntada = {};
                    $scope.submited = true;

                // TODO: clear all form inputs
                }
                catch (e) {
                    return;
                }
            }
           

        }])

    .controller('contactsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('notificationsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('phaseACtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('votationCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('themeExampleCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('phaseBCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('generalScheduleListCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('generalScheduleDetailsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('specificScheduleListCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('specificScheduleListConflictCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('addGuestsCtrl', ['$scope', 'newJuntadaService',
        function ($scope, newJuntadaService) {
            // retrieved data
            var data = [{
                contacts: [{ userId: 1, name: 'Franco Donadio' }, { userId: 2, name: 'Guido Donadio' }, { userId: 3, name: 'Santiago Z�rate' }]
            }];

            $scope.contacts = data[0].contacts;

            $scope.juntada = newJuntadaService.getCurrentNewJuntada();

            // if guests where already set, get them
            $scope.guests = $scope.juntada.guests ? $scope.juntada.guests : {};

            // use service to store juntada data between views/controllers
            $scope.addGuests = function () {
                newJuntadaService.addGuests($scope.guests);
            }

        }])

    .controller('arrangedJuntadaCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {

        }])

    .controller('specificScheduleDetailsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('specificScheduleConflictDetailsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('createGroupACtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('createGroupBCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('addImageCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])
    .controller('mapController', ['$scope', '$stateParams', 'mapService', function ($scope, $stateParams, mapService) {
        $scope.map = '';
        //var params = {
        //    center: new google.maps.LatLng(-34.6346748, -58.4385921),
        //    zoom: 17,
        //    mapTypeId: google.maps.MapTypeId.ROADMAP
        //};
        var address = 'De Las Garant�as 1286';
        mapService.setLocation($scope, address);
    }]);
 