﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Juntadas.Model;
using System.Web.Cors;
using System.Web.Http.Cors;

namespace JuntadasWebApi.Controllers
{
    public class JuntadasController : ApiController
    {

        public JuntadasController()
        {

        }

        [HttpGet]
        public string Juntadas()
        {
            return "juntadas!";
        }

        [HttpGet]
        [Route("Api/Juntadas/Check")]
        public string Check()
        {
            return "juntadas!";
        }

        // creation of juntada and initial stage configuration
        [HttpPost]
        //[EnableCors(origins: "http://localhost:4400", headers: "*", methods: "POST")]
        [Route("Api/Juntadas/Init/{juntada}")]
        public HttpResponseMessage Init([FromBody]Juntada juntada)
        {

            Console.WriteLine(juntada);
            HttpStatusCode status = HttpStatusCode.Accepted;
            return new HttpResponseMessage(status);
            // create juntada
            // show hints and neccesary notifications for the members
        }

        // This method is called after a user posts his schedule choices for the current stage, 
        // if the user is the last in the UsersDoneCallStack, then the proper method is called in order to resolve the Stage
        [Route("Api/Juntadas/ResolveAction")]
        public void ResolveAction (JuntadaAction action)
        {
            // check the current stage state and executes a strategy for that state
            StageCheckResult stageState = StageCheck(action.Juntada.Stage, action.ActiveMember);

            
            switch (stageState.ResultId)
            {
                case 0:
                    // state 0: stage without issues, validate and commit choices to db and wait for the next member
                    Commit(action);
                    break;
                // state 1: stage ends, commit choices to db and resolve stage
                case 1:
                    CommitAndResolve(action);
                    break;    

                
            }
        }

        private void CommitAndResolve(JuntadaAction action)
        {
            // validate and commit choices
            Commit(action);
            // check rounds (max 2)
            bool finalRound = false;
            if (action.Juntada.Stage.StageRound >= 2)
            {
                finalRound = true;
            }
            // resolve stage: (success, fail, if fail go to a new round? display proper notifications to sugests different behaviour for the next round? etc..
            // trigger stored procedure that takes an JuntadaAction and returns a StageResult with a List<TimePeriods> that are members schedule coincidences based on the stage type rules
            // resolve stage failed 1: check if one more round is available, ask if members can flexibilize their schedules for a new round
            // resolve stage failed 2: if new round, set inital stage state + UI hints
            throw new NotImplementedException();
        }

        private void Commit(JuntadaAction action)
        {
            throw new NotImplementedException();
        }

        // Checks the current stage state, that state is expressed with a stageCheckResult.ResultId.
        // Then runs the proper method to either go to the next stage, resolve the juntada, etc...
        private StageCheckResult StageCheck(Stage stage, Member member)
        {
            // creates a stage result object to be completed through the check
            StageCheckResult stageCheckResult = new StageCheckResult();

            bool lastUser = LastUserCheck(stage, member);

            if (/*lastUser & other checks*/ lastUser & stage.FinalStage)
            {
                stageCheckResult.ResultId = 0;
            } // else { if there is any inconsistency notify/solve, otherwise continue with next stage }

            return stageCheckResult;
        }

        // HARDCODED
        private bool LastUserCheck(Stage stage, Member member)
        {
            List<Member> MembersCompletedStage = stage.Members.FindAll(m => m.StageCompleted == true);
            List<Member> PendingMembers = stage.Members.FindAll(m => m.StageCompleted == false);
            
            // checks if there is only one member pending to complete the stage and that member is the one we are checking now
            if (MembersCompletedStage.Count == stage.Members.Count - 1 && PendingMembers.Count == 1 && PendingMembers.Contains(member))
            {
                return true;
            } else { 
                return false;
            };


        }
    }
}
