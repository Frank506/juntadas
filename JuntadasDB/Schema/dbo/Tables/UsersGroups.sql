﻿CREATE TABLE [dbo].[UsersGroups]
(
	[UsersGroupId] INT NOT NULL IDENTITY (1,1),
	[UsersGroupType] INT NOT NULL,
	[Name] VARCHAR(20) NULL
);
