﻿CREATE TABLE [dbo].[StageTypes]
(
	[StageTypeId] INT NOT NULL IDENTITY (1,1),
	[Name] VARCHAR(20) NOT NULL,
	[StagePosition] TINYINT NOT NULL -- determines the relative order of the stage, 0, 1, 2, 3, etc... when adding a new stage to the system it's easier to change this number than changin Id's
);
