﻿-- Simple members grouping for different purpouses (absents, lateness, etc...)
CREATE TABLE [dbo].[MembersGroups]
(
	[MembersGroupId] INT NOT NULL IDENTITY (1,1),
	[Flag] BIT NOT NULL,
	[MemberGroupName] VARCHAR(20) NULL
);
