﻿CREATE TABLE [dbo].[TimePeriodTypes] -- Period Types defines a timespan in the day
(
	[TimePeriodTypeId] INT NOT NULL IDENTITY (1,1),
	[Name] VARCHAR(20) NOT NULL,
	[Start] TIME NOT NULL,
	[End] TIME NOT NULL
);
