﻿CREATE TABLE [dbo].[Stages]
(
	[StageId] INT NOT NULL IDENTITY (1,1),
	[StageTypeId] INT NOT NULL,
	[StageName] VARCHAR(20) NOT NULL,
	[StageRound] INT NOT NULL,
	[MembersGroupId] INT NOT NULL
);
