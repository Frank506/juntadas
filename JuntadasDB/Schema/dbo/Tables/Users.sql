﻿CREATE TABLE [dbo].[Users]
(
	[UserId] INT NOT NULL IDENTITY (1,1),
	[UserName] VARCHAR(20) NOT NULL,
	[UserRoleId] INT NOT NULL,
	[UserProfileId] INT NOT NULL
);
