﻿CREATE TABLE [dbo].[StageResults]
(
	[StageResultsId] INT NOT NULL IDENTITY (1,1),
	[StageId] INT NOT NULL,
	[TimePeriodId] INT NOT NULL
);
