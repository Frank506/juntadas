﻿-- TODO: implementar FKs una vez este claro como se van a manejar las closed juntadas
CREATE TABLE [dbo].[ClosedJuntadas]
(
	[ClosedJuntadaId] INT NOT NULL IDENTITY (1,1),
	[JuntadaId] INT NOT NULL,
	[Name] VARCHAR(20) NOT NULL,
	[Address] VARCHAR(200) NOT NULL,
	[CreatorId] INT NOT NULL,
	[Start] DATETIME NOT NULL,
	[End] DATETIME NOT NULL,
	[Timeless] BIT NOT NULL,
	[RecievePersonally] BIT NOT NULL,
	[LateArrivalsId] INT NOT NULL,
	[DeclaredAbsencesGroupId] BIT NOT NULL,
	[Notes] VARCHAR NULL
);
