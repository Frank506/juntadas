﻿CREATE TABLE [dbo].[UsersGroupTypes]
(
	[UsersGroupTypeId] INT NOT NULL IDENTITY (1,1),
	[Name] VARCHAR(20) NOT NULL
);
