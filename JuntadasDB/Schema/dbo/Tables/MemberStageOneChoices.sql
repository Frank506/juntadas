﻿CREATE TABLE [dbo].[MemberStageOneChoices]
(
	[MemberStageOneChoicesId] INT NOT NULL IDENTITY (1,1),
	[JuntadaId] INT NOT NULL,
	[UserId] INT NOT NULL,
	[MemberId] INT NOT NULL,
	[Date] DATETIME NOT NULL,
	[TimePeriodId] INT NOT NULL
);
