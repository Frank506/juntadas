﻿CREATE TABLE [dbo].[Juntadas]
(
	[JuntadaId] INT NOT NULL IDENTITY (1,1),
	[Name] VARCHAR(20) NOT NULL,
	[Address] VARCHAR(200) NOT NULL,
	[PresetId] INT NOT NULL,
	[DaysRangeFrom] DATETIME NOT NULL,
	[DaysRangeTo] DATETIME NOT NULL,
	[Duration] TIME NOT NULL,
	[Timeless] BIT NOT NULL,
	[RecievePersonally] BIT NOT NULL,
	[OtherHosts] BIT NOT NULL,
	[LateArrivals] BIT NOT NULL,
	[LateArrivalsLateness] TIME NULL,
	[Absences] BIT NOT NULL,
	[AbsencesHowManyOrWho] BIT NULL,
	[Absences_Who_CanBeAbscentGroupId] INT NULL,
	[Absences_HowMany] TINYINT NULL,
	[StageId] INT NOT NULL,
	[Closed] BIT NOT NULL
);
