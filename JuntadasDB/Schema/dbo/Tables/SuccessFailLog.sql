﻿CREATE TABLE [dbo].[SuccessFailLog]
(
	[SuccessFailId] INT NOT NULL IDENTITY (1,1),
	[JuntadaId] INT NOT NULL,
	[DateTime] DATETIME NOT NULL,
	[Log] VARCHAR NOT NULL
);
