﻿-- Simple members grouping for different purpouses (absents, lateness, etc...)
CREATE TABLE [dbo].[MembersMembersGroups]
(
	[MembersMembersGroupId] INT NOT NULL IDENTITY (1,1),
	[MembersGroupId] INT NOT NULL,
	[MemberId] INT NOT NULL
);
