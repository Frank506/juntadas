﻿CREATE TABLE [dbo].[Members]
(
	[MemberId] INT NOT NULL IDENTITY (1,1),
	[UserId] INT NOT NULL,
	[JuntadaId] INT NOT NULL,
	[Host] BIT NOT NULL,
	[CanBeAbsent] BIT NOT NULL,
	[IsAbsent] BIT NOT NULL,
	[Name] VARCHAR(20) NULL
);
