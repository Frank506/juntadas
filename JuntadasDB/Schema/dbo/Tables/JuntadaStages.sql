﻿CREATE TABLE [dbo].[JuntadaStages]
(
	[JuntadaStageId] INT NOT NULL IDENTITY (1,1),
	[Name] VARCHAR(20) NOT NULL,
	[StageNumber] INT NOT NULL
);
