﻿CREATE TABLE [dbo].[TimePeriods] -- System definition of time periods, its a general time of the day that can be defined with more precision through the process or discarded
(
	[TimePeriodId] INT NOT NULL IDENTITY (1,1),
	[StageId] INT NOT NULL, -- we need this if we have a time periods group table
	[JuntadaId] INT NOT NULL, -- we need this if we have a time periods group table
	[TimePeriodTypeId] INT NOT NULL, -- we need this if we have a time periods group table
	[SpecificTime] TIME NULL,
	[Date] DATETIME NOT NULL,
);
