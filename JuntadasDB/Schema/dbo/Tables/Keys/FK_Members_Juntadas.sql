﻿ALTER TABLE [dbo].[Members]
ADD CONSTRAINT [FK_Members_Juntadas]
    FOREIGN KEY ([JuntadaId])
    REFERENCES [dbo].[Juntadas]
        ([JuntadaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
