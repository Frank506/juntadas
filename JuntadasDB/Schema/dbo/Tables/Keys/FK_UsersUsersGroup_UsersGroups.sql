﻿ALTER TABLE [dbo].[UsersUsersGroups]
ADD CONSTRAINT [FK_UsersUsersGroup_UsersGroups]
    FOREIGN KEY ([UsersGroupsId])
    REFERENCES [dbo].[UsersGroups]
        ([UsersGroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
