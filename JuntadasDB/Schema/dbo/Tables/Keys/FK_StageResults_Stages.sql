﻿ALTER TABLE [dbo].[StageResults]
ADD CONSTRAINT [FK_StageResults_Stages]
    FOREIGN KEY ([StageId])
    REFERENCES [dbo].[Stages]
        ([StageId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
