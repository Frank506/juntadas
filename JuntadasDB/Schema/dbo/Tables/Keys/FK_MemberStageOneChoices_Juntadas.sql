﻿ALTER TABLE [dbo].[MemberStageOneChoices]
ADD CONSTRAINT [FK_MemberStageOneChoices_Juntadas]
    FOREIGN KEY ([JuntadaId])
    REFERENCES [dbo].[Juntadas]
        ([JuntadaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
