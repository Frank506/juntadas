﻿ALTER TABLE [dbo].[MemberStageTwoChoices]
ADD CONSTRAINT [FK_MemberStageTwoChoices_TimePeriods]
    FOREIGN KEY ([TimePeriodId])
    REFERENCES [dbo].[TimePeriods]
        ([TimePeriodId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
