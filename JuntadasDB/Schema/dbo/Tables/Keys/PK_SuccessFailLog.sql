﻿ALTER TABLE [dbo].[SuccessFailLog]
ADD CONSTRAINT [PK_SuccessFailLog]
    PRIMARY KEY CLUSTERED ([SuccessFailId] ASC);