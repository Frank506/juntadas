﻿ALTER TABLE [dbo].[UsersUsersGroups]
ADD CONSTRAINT [FK_UsersUsersGroup_Users]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
