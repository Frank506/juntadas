﻿ALTER TABLE [dbo].[MemberStageOneChoices]
ADD CONSTRAINT [FK_MemberStageOneChoices_TimePeriods]
    FOREIGN KEY ([TimePeriodId])
    REFERENCES [dbo].[TimePeriods]
        ([TimePeriodId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
