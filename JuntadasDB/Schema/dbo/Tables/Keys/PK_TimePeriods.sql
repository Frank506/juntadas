﻿ALTER TABLE [dbo].[TimePeriods]
ADD CONSTRAINT [PK_TimePeriods]
    PRIMARY KEY CLUSTERED ([TimePeriodId] ASC);