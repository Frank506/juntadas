﻿ALTER TABLE [dbo].[Juntadas]
ADD CONSTRAINT [FK_Juntadas_Stages]
    FOREIGN KEY ([StageId])
    REFERENCES [dbo].[Stages]
        ([StageId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
