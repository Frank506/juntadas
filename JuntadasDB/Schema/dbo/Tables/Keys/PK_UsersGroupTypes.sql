﻿ALTER TABLE [dbo].[UsersGroupTypes]
ADD CONSTRAINT [PK_UsersGroupTypes]
    PRIMARY KEY CLUSTERED ([UsersGroupTypeId] ASC);