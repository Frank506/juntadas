﻿ALTER TABLE [dbo].[MemberStageOneChoices]
ADD CONSTRAINT [FK_MemberStageOneChoices_Members]
    FOREIGN KEY ([MemberId])
    REFERENCES [dbo].[Members]
        ([MemberId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
