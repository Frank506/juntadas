﻿ALTER TABLE [dbo].[MembersMembersGroups]
ADD CONSTRAINT [FK_MembersMembersGroups_MembersGroups]
    FOREIGN KEY ([MembersGroupId])
    REFERENCES [dbo].[MembersGroups]
        ([MembersGroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
