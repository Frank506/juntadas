﻿ALTER TABLE [dbo].[StageTypes]
ADD CONSTRAINT [PK_StageTypes]
    PRIMARY KEY CLUSTERED ([StageTypeId] ASC);