﻿ALTER TABLE [dbo].[MembersMembersGroups]
ADD CONSTRAINT [FK_MembersMembersGroups_Members]
    FOREIGN KEY ([MemberId])
    REFERENCES [dbo].[Members]
        ([MemberId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
