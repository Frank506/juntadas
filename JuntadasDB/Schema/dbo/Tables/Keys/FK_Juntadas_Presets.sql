﻿ALTER TABLE [dbo].[Juntadas]
ADD CONSTRAINT [FK_Juntadas_Presets]
    FOREIGN KEY ([PresetId])
    REFERENCES [dbo].[Presets]
        ([PresetId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
