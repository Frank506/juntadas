﻿ALTER TABLE [dbo].[TimePeriodTypes]
ADD CONSTRAINT [PK_TimePeriodTypes]
    PRIMARY KEY CLUSTERED ([TimePeriodTypeId] ASC);