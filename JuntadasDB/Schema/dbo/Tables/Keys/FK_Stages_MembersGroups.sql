﻿ALTER TABLE [dbo].[Stages]
ADD CONSTRAINT [FK_Stages_MembersGroups]
    FOREIGN KEY ([MembersGroupId])
    REFERENCES [dbo].[MembersGroups]
        ([MembersGroupId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
