﻿ALTER TABLE [dbo].[MemberStageOneChoices]
ADD CONSTRAINT [FK_MemberStageOneChoices_Users]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
