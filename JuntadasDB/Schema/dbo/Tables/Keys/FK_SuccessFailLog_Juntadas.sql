﻿ALTER TABLE [dbo].[SuccessFailLog]
ADD CONSTRAINT [FK_SuccessFailLog_Juntadas]
    FOREIGN KEY ([JuntadaId])
    REFERENCES [dbo].[Juntadas]
        ([JuntadaId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
