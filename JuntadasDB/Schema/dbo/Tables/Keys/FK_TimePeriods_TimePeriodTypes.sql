﻿ALTER TABLE [dbo].[TimePeriods]
ADD CONSTRAINT [FK_TimePeriods_TimePeriodTypes]
    FOREIGN KEY ([TimePeriodTypeId])
    REFERENCES [dbo].[TimePeriodTypes]
        ([TimePeriodTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
