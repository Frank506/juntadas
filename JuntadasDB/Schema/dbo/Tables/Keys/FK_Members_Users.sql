﻿ALTER TABLE [dbo].[Members]
ADD CONSTRAINT [FK_Members_Users]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
