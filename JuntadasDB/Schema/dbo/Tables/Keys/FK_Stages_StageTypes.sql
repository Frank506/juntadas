﻿ALTER TABLE [dbo].[Stages]
ADD CONSTRAINT [FK_Stages_StageTypes]
    FOREIGN KEY ([StageTypeId])
    REFERENCES [dbo].[StageTypes]
        ([StageTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
