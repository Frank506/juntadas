﻿ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_Users_UsersRoles]
    FOREIGN KEY ([UserRoleId])
    REFERENCES [dbo].[UsersRoles]
        ([UserRoleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
