﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Juntadas.Rest.Controllers
{
    public class JuntadasController : ApiController
    {
        public JuntadasController ()
        {

        }

        [Route("api/juntadas")]
        public string Get()
        {
            return "juntadas";
        }
    }
}
